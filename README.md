These templates are released under the GNU WTFPL.

If you use them drop me a line to my email.

They are provided as is, with no promise of support. Unless you hire me. :)

The pattern images included were downloaded from subltepatterns.com.
Go and check them out.

Check out my blog.  http://orangethirty.blogspot.com
